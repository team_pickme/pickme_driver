package com.dev4u.pickme.driver.model;

public class Driver {
    private String id;//generado por firebase
    private String name;//nombre completo
    private String address;//direccion de domicilio
    private String password;
    private String documentId;//dui o documento identificativo
    private String phoneNumber;//numero de telefono
    private String driverLicense;//licencia de conducir
    private Vehicle vehicle;//informacion de vehiculo asociada al conductor

    public Driver() {
    }
    public Driver(String phoneNumber,String password ) {
        this.password = password;
        this.phoneNumber = phoneNumber;
    }
    public Driver(String name, String address, String password, String documentId, String phoneNumber, String driverLicense, Vehicle vehicle) {
        this.name = name;
        this.address = address;
        this.password = password;
        this.documentId = documentId;
        this.phoneNumber = phoneNumber;
        this.driverLicense = driverLicense;
        this.vehicle = vehicle;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getDocumentId() {
        return documentId;
    }
    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getDriverLicense() {
        return driverLicense;
    }
    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense;
    }
    public Vehicle getVehicle() {
        return vehicle;
    }
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
}
