package com.dev4u.pickme.driver.login.presenter;

import android.util.Log;

import com.dev4u.pickme.driver.model.LoginResult;
import com.dev4u.pickme.driver.model.LoginStatus;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class LoginInteractor {
    LoginPresenter presenter;

    CollectionReference usersPhoneNumbersCollection = FirebaseFirestore.getInstance().collection("usersPhoneNumbers");

    public LoginInteractor(LoginPresenter presenter) {
        this.presenter = presenter;
    }

    public void processLogin(String user,String password){
        if(user.isEmpty() || password.isEmpty()){
            presenter.login(new LoginResult(LoginStatus.EMPTY_VALUES));
        }else{
            usersPhoneNumbersCollection
                    .document(user)
                    .get()
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            DocumentSnapshot document = task.getResult();
                            if(document.exists()){
                                Log.d("INTERACTOR","exist");
                                validatePassword(document.getDocumentReference("userId"),password);
                            }else{
                                presenter.login(new LoginResult(LoginStatus.PASSWORD_INCORRECT));
                                Log.d("INTERACTOR","no exist");
                            }
                        }else{
                            presenter.login(new LoginResult(LoginStatus.SERVER_ERROR));
                            Log.d("INTERACTOR",task.getException().getMessage());
                        }
                    });

        }
    }

    private void validatePassword(DocumentReference reference,String password){
        reference.get().addOnCompleteListener(task -> {
           if(task.isSuccessful()){
               DocumentSnapshot document = task.getResult();
               if(document.exists()){
                   //si la contraseña coincide se guarda el documento
                   if(password!=null && document.getString("password").equals(password)){
                       presenter.login(new LoginResult(document, LoginStatus.OK));
                   }else{
                       presenter.login(new LoginResult(LoginStatus.PASSWORD_INCORRECT));
                   }
               }else{
                   //si esta el telefono pero no esta la informacion, error de base de datos
                   presenter.login(new LoginResult(LoginStatus.SERVER_ERROR));
               }
           }else{
               presenter.login(new LoginResult(LoginStatus.SERVER_ERROR));
               Log.d("INTERACTOR",task.getException().getMessage());
           }
        });
    }
}
