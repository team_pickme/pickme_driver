package com.dev4u.pickme.driver.account.view.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.dev4u.pickme.driver.R;
import com.dev4u.pickme.driver.account.view.AccountActivity;
import com.dev4u.pickme.driver.main.view.MainActivity;
import com.google.common.io.LineReader;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountSecondDetailFragment extends Fragment {


    private LinearLayout btnSave;

    public AccountSecondDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_second_detail, container, false);

        btnSave = view.findViewById(R.id.btn_account_save);

        btnSave.setOnClickListener(click->{
            Intent intent = new Intent(getContext(), MainActivity.class);
            startActivity(intent);
        });

        return view;
    }

}
