package com.dev4u.pickme.driver.account.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.dev4u.pickme.driver.R;
import com.dev4u.pickme.driver.account.view.fragment.AccountFirstDetailFragment;
import com.dev4u.pickme.driver.account.view.fragment.AccountSecondDetailFragment;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class AccountActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private CircleIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        getSupportActionBar().hide();
        init();
    }
    private void init(){
        viewPager = findViewById(R.id.pager_introduction);
        indicator = findViewById(R.id.indicator);
        setupViewPager(viewPager);
        indicator.setViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        AccountFirstDetailFragment accountFirst     = new AccountFirstDetailFragment();
        AccountSecondDetailFragment accountSecond   = new AccountSecondDetailFragment();
        adapter.addFragment(accountFirst);
        adapter.addFragment(accountSecond);
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }
        @Override
        public int getCount() {
            return mFragmentList.size();
        }
        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }
}
