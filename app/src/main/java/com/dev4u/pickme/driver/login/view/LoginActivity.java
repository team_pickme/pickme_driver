package com.dev4u.pickme.driver.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.dev4u.pickme.driver.R;
import com.dev4u.pickme.driver.account.view.AccountActivity;
import com.dev4u.pickme.driver.login.contract.LoginContract;
import com.dev4u.pickme.driver.helper.SnackBarHelper;
import com.dev4u.pickme.driver.model.LoginResult;
import com.dev4u.pickme.driver.model.LoginStatus;
import com.dev4u.pickme.driver.login.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    private LoginPresenter presenter;

    private CoordinatorLayout rootView;
    private EditText txtTel;
    private EditText txtPass;
    private Button btnLogin;
    private TextView lblCreateAccount;
    private TextView lblHelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        presenter = new LoginPresenter(this);
        getSupportActionBar().hide();
        init();
    }

    private void init(){
        rootView    = findViewById(R.id.layout_login);
        txtTel      = findViewById(R.id.txt_login_tel);
        txtPass     = findViewById(R.id.txt_login_pass);
        btnLogin    = findViewById(R.id.btn_login);
        lblHelp     = findViewById(R.id.lbl_login_help);
        lblCreateAccount = findViewById(R.id.lbl_login_create_account);
        //eventos
        btnLogin.setOnClickListener(click-> presenter.login(txtTel.getText().toString(),txtPass.getText().toString()));
        lblCreateAccount.setOnClickListener(click->{
            Intent intent = new Intent(LoginActivity.this, AccountActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void login(LoginResult result) {
        if(result.getLoginStatus()== LoginStatus.OK){
            String name = result.getUserSnapshot().getString("name");
            SnackBarHelper.showSnackBar(rootView,"Bienvenido "+name);
            //proceder a la siguiente activity
        }else{
            SnackBarHelper.showWarningSnackBar(rootView,result.getLoginStatus().getValue());
        }
    }
}
