package com.dev4u.pickme.driver.main.view;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.dev4u.pickme.driver.R;
import com.dev4u.pickme.driver.main.view.fragment.HistoryFragment;
import com.dev4u.pickme.driver.main.view.fragment.MapFragment;
import com.dev4u.pickme.driver.main.view.fragment.ProfileFragment;
import com.dev4u.pickme.driver.main.view.fragment.SettingsFragment;
import com.dev4u.pickme.driver.util.BaseFragment;
import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

public class MainActivity extends AppCompatActivity implements BaseFragment.FragmentNavigation, FragNavController.TransactionListener, FragNavController.RootFragmentListener{



    private final int INDEX_MAP         = FragNavController.TAB1;
    private final int INDEX_HISTORY     = FragNavController.TAB2;
    private final int INDEX_PROFILE     = FragNavController.TAB3;
    private final int INDEX_SETTINGS    = FragNavController.TAB4;

    private static MapFragment frm1         = new MapFragment();
    private static HistoryFragment frm2     = new HistoryFragment();
    private static ProfileFragment frm3     = new ProfileFragment();
    private static SettingsFragment frm4    = new SettingsFragment();

    private BottomBar mBottomBar;
    private FragNavController mNavController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        mBottomBar = findViewById(R.id.bottomBar);
        mBottomBar.selectTabAtPosition(0);

        mNavController = FragNavController.newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.container)
                .transactionListener(this)
                .rootFragmentListener(this, 4)
                .defaultTransactionOptions(FragNavTransactionOptions.newBuilder().customAnimations(R.anim.alpha_in, R.anim.alpha_out,R.anim.alpha_in,R.anim.alpha_out).build())
                .build();

        mBottomBar.setOnTabSelectListener(tabId -> {
            switch (tabId) {
                case R.id.bb_menu_map:
                    mNavController.switchTab(INDEX_MAP);
                    break;
                case R.id.bb_menu_history:
                    mNavController.switchTab(INDEX_HISTORY);
                    break;
                case R.id.bb_menu_profile:
                    mNavController.switchTab(INDEX_PROFILE);
                    break;
                case R.id.bb_menu_settings:
                    mNavController.switchTab(INDEX_SETTINGS);
                    break;
            }
        });

        mBottomBar.setOnTabReselectListener(tabId -> mNavController.clearStack());
    }

    @Override
    public void onBackPressed() {
        //se elimina de la pila de fragmentos
        if (!mNavController.isRootFragment()) {
            Log.d("Eliminado de pila"," pos :"+mNavController.getCurrentStackIndex());
            mNavController.popFragment();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void pushFragment(Fragment fragment) {
        if (mNavController != null) {
            mNavController.pushFragment(fragment);
        }
    }


    @Override
    public Fragment getRootFragment(int i) {
        switch (i) {
            case INDEX_MAP:
                return frm1;
            case INDEX_HISTORY:
                return frm2;
            case INDEX_PROFILE:
                return frm3;
            case INDEX_SETTINGS:
                return frm4;
        }
        throw new IllegalStateException("Need to send an index that we know");
    }

    @Override
    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType transactionType) {

    }

    @Override
    public void onTabTransaction(Fragment fragment, int i) {

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mNavController != null) {
            mNavController.onSaveInstanceState(outState);
        }
    }
}

