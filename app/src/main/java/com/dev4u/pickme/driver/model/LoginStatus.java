package com.dev4u.pickme.driver.model;

public enum LoginStatus {
    OK("OK"),
    EMPTY_VALUES("Falta completar datos"),
    PASSWORD_INCORRECT("Usuario o contraseña incorrecta"),
    NO_INTERNET("Activa la conexión a internet"),
    SERVER_ERROR("Error en el servidor");

    private String value;
    LoginStatus(String value){
        this.value=value;
    }
    public String getValue(){
        return this.value;
    }
}
