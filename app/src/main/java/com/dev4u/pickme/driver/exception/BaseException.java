package com.dev4u.pickme.driver.exception;

import com.dev4u.pickme.driver.model.LoginStatus;

public abstract class BaseException extends Exception {
    private LoginStatus loginStatus;
    public BaseException(LoginStatus loginStatus){
        this.loginStatus = loginStatus;
    }
    public String getInfo(){
        return loginStatus.getValue();
    }
}
