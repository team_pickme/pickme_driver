package com.dev4u.pickme.driver.login.contract;

import com.dev4u.pickme.driver.model.LoginResult;

public interface LoginContract {
    interface View {
        void login(LoginResult result);
    }
    interface Presenter {
        void login(LoginResult result);
        void login(String user,String password);
    }
}
