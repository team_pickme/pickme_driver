package com.dev4u.pickme.driver.model;

import com.google.firebase.firestore.DocumentSnapshot;

import java.io.Serializable;

public class LoginResult implements Serializable {
    private DocumentSnapshot userSnapshot;
    private LoginStatus loginStatus;

    public LoginResult() {
        this.loginStatus =null;
    }
    public LoginResult(LoginStatus loginStatus) {
        this.loginStatus = loginStatus;
    }

    public LoginResult(DocumentSnapshot userSnapshot, LoginStatus loginStatus) {
        this.userSnapshot = userSnapshot;
        this.loginStatus = loginStatus;
    }

    public DocumentSnapshot getUserSnapshot() {
        return userSnapshot;
    }

    public void setUserSnapshot(DocumentSnapshot userSnapshot) {
        this.userSnapshot = userSnapshot;
    }

    public LoginStatus getLoginStatus() {
        return loginStatus;
    }
    public void setLoginStatus(LoginStatus loginStatus) {
        this.loginStatus = loginStatus;
    }
}
