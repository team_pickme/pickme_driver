package com.dev4u.pickme.driver.model;

public class Vehicle {

    private String model;
    private String vehicleLicensePlace;//placa de vehiculo
    private Integer year;
    private String engineCapacity;

    public Vehicle() {
    }

    public Vehicle(String model, String vehicleLicensePlace, Integer year, String engineCapacity) {
        this.model = model;
        this.vehicleLicensePlace = vehicleLicensePlace;
        this.year = year;
        this.engineCapacity = engineCapacity;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public String getVehicleLicensePlace() {
        return vehicleLicensePlace;
    }
    public void setVehicleLicensePlace(String vehicleLicensePlace) {
        this.vehicleLicensePlace = vehicleLicensePlace;
    }
    public Integer getYear() {
        return year;
    }
    public void setYear(Integer year) {
        this.year = year;
    }
    public String getEngineCapacity() {
        return engineCapacity;
    }
    public void setEngineCapacity(String engineCapacity) {
        this.engineCapacity = engineCapacity;
    }
}
