package com.dev4u.pickme.driver.helper;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.dev4u.pickme.driver.R;
import com.google.android.material.snackbar.Snackbar;

public class SnackBarHelper {
    public static void showSnackBar(View rootView, String msg){
        Snackbar snack  = Snackbar.make(rootView, msg, Snackbar.LENGTH_LONG);
        View view       = snack.getView();

        TextView tv     = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }

    public static void showWarningSnackBar(View rootView, String msg){
        Snackbar snack  = Snackbar.make(rootView, msg, Snackbar.LENGTH_LONG);
        View view       = snack.getView();
        TextView tv     = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(rootView.getResources().getColor(R.color.warning));
        snack.show();
    }

}
