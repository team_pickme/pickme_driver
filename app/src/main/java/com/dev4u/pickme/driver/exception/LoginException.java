package com.dev4u.pickme.driver.exception;

import com.dev4u.pickme.driver.model.LoginStatus;

public class LoginException extends BaseException {
    public LoginException(LoginStatus loginStatus) {
        super(loginStatus);
    }
}
