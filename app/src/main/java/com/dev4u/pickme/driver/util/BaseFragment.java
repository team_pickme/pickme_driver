package com.dev4u.pickme.driver.util;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class BaseFragment extends Fragment {


    public static final String ARGS_INSTANCE = "com.dev4u.pickme.driver.argsInstance";

    FragmentNavigation mFragmentNavigation;
    int mInt = 0;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            mInt = args.getInt(ARGS_INSTANCE);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentNavigation) {
            mFragmentNavigation = (FragmentNavigation) context;
        }
    }


    public interface FragmentNavigation {
        public void pushFragment(Fragment fragment);
    }
}
