package com.dev4u.pickme.driver.login.presenter;

import com.dev4u.pickme.driver.login.contract.LoginContract;
import com.dev4u.pickme.driver.model.LoginResult;
import com.dev4u.pickme.driver.model.LoginStatus;

public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View view;
    private LoginInteractor interactor;

    public LoginPresenter(final LoginContract.View view){
        this.view = view;
        interactor = new LoginInteractor(this);
    }

    @Override
    public void login(LoginResult result) {
        try {
            view.login(result);
        }catch (Exception e){
            view.login(new LoginResult(LoginStatus.SERVER_ERROR));
        }
    }

    @Override
    public void login(String user, String password) {
        interactor.processLogin(user,password);
    }
}
